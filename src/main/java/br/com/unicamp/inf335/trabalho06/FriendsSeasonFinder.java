package br.com.unicamp.inf335.trabalho06;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * Hello world!
 *
 */
public class FriendsSeasonFinder {
	public static void main(String[] args) {
		Double inputValue = displayMenu();
		MongoClient mongoClient = null;
		mongoClient = new MongoClient();
		MongoDatabase friends = mongoClient.getDatabase("friends");
		MongoCollection<Document> collection = friends.getCollection("samples_friends");
		FindIterable<Document> selected = collection.find(new Document("season", inputValue));
		System.out.println("#################Títulos contidos nesta temporada:###############");
		for (Document document : selected) {
			System.out.println(document.get("name"));
		}
		System.out.println("#################################################################");
		mongoClient.close();
	}

	@SuppressWarnings("resource")
	public static Double displayMenu() {
		Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
		mongoLogger.setLevel(Level.SEVERE);
		System.out.println("Buscador de temporadas de friends!");
		System.out.println("Qual temporada você deseja consultar?");
		String inputUser = new Scanner(System.in).nextLine();
		Double targetSeason = 1D;
		try {
			targetSeason = Double.valueOf(inputUser);
		} catch (NumberFormatException nfException) {
			System.out.println(String.format("O número fornecido não é valido! Assumindo busca de temporada %.0f como padrão", targetSeason));
		}
		return targetSeason;
	}
}
